package br.com.crawlerglobo.model;

import java.io.Serializable;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * @author jgosilva
 *
 */
public class Description implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String type; 
	
	private String content;
	
	@XStreamAlias("content")
	private List<String> contents;
	
	public void setType(String type) {
		this.type = type;
	}
	public void setContent(String content) {
		this.content = content;
	}

	public void setContents(List<String> contents) {
		this.contents = contents;
	}
	
	
	
	
}
