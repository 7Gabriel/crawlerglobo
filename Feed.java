package br.com.crawlerglobo.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


/**
 * @author jgosilva
 *
 */
public class Feed implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private final List<Item> item = new ArrayList<Item>();
    
    public List<Item> getMessages() {
        return item;
    }

	@Override
	public String toString() {
		return "Feed [item=" + item + "]";
	}

  
}
