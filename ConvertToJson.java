package br.com.crawlerglobo.parser;

import java.io.Writer;

import org.apache.log4j.Logger;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;
import com.thoughtworks.xstream.io.json.JsonHierarchicalStreamDriver;
import com.thoughtworks.xstream.io.json.JsonWriter;

import br.com.crawlerglobo.model.Feed;

/**
 * @author jgosilva
 *
 */
public class ConvertToJson {

	private static final Logger logger = Logger.getLogger(ConvertToJson.class);
	
	public static String convertParaJson(Feed feed) {
		logger.info("[ConvertToJson][convertParaJson()] iniciando convers�o de Json");
		XStream xstream = new XStream(new JsonHierarchicalStreamDriver() {
	            public HierarchicalStreamWriter createWriter(Writer writer) {
	                return new JsonWriter(writer, JsonWriter.STRICT_MODE);
	            }
	        });
	        xstream.autodetectAnnotations(true);
	        xstream.setMode(XStream.XPATH_RELATIVE_REFERENCES);
	        xstream.alias(feed.getClass().getSimpleName(),feed.getClass());
	        return xstream.toXML(feed);
	}
}
