package br.com.crawlerglobo.parser;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import br.com.crawlerglobo.model.Description;
import br.com.crawlerglobo.uil.CrawlerConstants;

public class ConvertHtml {
	
		

	  	public static List<Description> getContentParagraph(String html) {
	    	List<Description> paragraphList = new ArrayList<Description>();
	    	Pattern pattern = Pattern.compile(CrawlerConstants.REGEX_TAG_P, Pattern.MULTILINE);
	    	Matcher match = pattern.matcher(html);
	        while(match.find()) {
	        	getDescriptionTextList(paragraphList, match);
	        }
			return paragraphList;
		}

	   public static void getDescriptionTextList(List<Description> paragraphList, Matcher match) {
			Description des = new Description();
			des.setType(CrawlerConstants.TYPE_TEXT);
			String tag = match.group(1);
			des.setContent(tag);
			paragraphList.add(des);
		}
	    
	    public static List<Description> getContentImg(String html) {
	        	List<Description> imageList = new ArrayList<Description>();
	        	Pattern pattern = Pattern.compile(CrawlerConstants.REGEX_TAG_IMG, Pattern.MULTILINE);
	            Matcher match = pattern.matcher(html);
	            while(match.find()) {
	            	getDescriptionImageList(imageList, match);
	            }
	    		return imageList;
	    	}

		public static void getDescriptionImageList(List<Description> imageList, Matcher match) {
				Description des = new Description();
				des.setType(CrawlerConstants.TYPE_IMAGE);
				Pattern pattern = Pattern.compile(CrawlerConstants.REGEX_IMAGE_LINK, Pattern.MULTILINE);
				String tag = match.group(1);
				Matcher matchHttp = pattern.matcher(tag);
				matchHttp.find();
				String http = matchHttp.group();
				int index = http.indexOf("\"");
				des.setContent(http.substring(0, index));
				imageList.add(des);
			}

	   public static List<Description> getContentUl(String html) {
	        	List<Description> ulList = new ArrayList<Description>();
	        	List<String> links = new ArrayList<String>();
	        		Pattern pattern = Pattern.compile(CrawlerConstants.REGEX_TAG_UL, Pattern.MULTILINE);
	                Matcher match = pattern.matcher(html);
	                while(match.find()) {
	                	Pattern ul = Pattern.compile(CrawlerConstants.REGEX_TAG_UL_LINK, Pattern.MULTILINE);
	                	String tag = match.group(1);
	                	Matcher matchUl = ul.matcher(tag);
	                	while(matchUl.find()){
	                		String link = matchUl.group().replaceFirst("href=\"", "").replaceFirst("\">", "").replaceFirst("\"[\\s]?target=\"[a-zA-Z_0-9]*", "");
	                		links.add(link);
	                	}
	                	getDescriptionUlList(ulList, match, links);
	                }
	                return ulList;
	        }

		public static void getDescriptionUlList(List<Description> ulList, Matcher m, List<String> links) {
				Description des = new Description();
				des.setType(CrawlerConstants.TYPE_UL_LINK);
				//String tag = m.group(1);
				des.setContents(links);
				ulList.add(des);
			}
}
