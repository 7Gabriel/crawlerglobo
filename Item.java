package br.com.crawlerglobo.model;

import java.io.Serializable;
import java.util.List;

/**
 * @author jgosilva
 *
 */
public class Item implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private String title;
	
    private String link;

    private List<Description> description;

    public void setTitle(String title) {
		this.title = title;
	}
	public List<Description> getDescription() {
		return description;
	}
	public void setDescription(List<Description> description) {
		this.description = description;
	}
	public void setLink(String link) {
		this.link = link;
	}
	public String toString() {
        return "FeedMessage [title=" + title + ", description=" + description
                + ", link=" + link + "]";
    }
}
