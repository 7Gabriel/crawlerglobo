package br.com.crawlerglobo.parser;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.XMLEvent;

import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;

import br.com.crawlerglobo.model.Description;
import br.com.crawlerglobo.model.Feed;
import br.com.crawlerglobo.model.Item;
import br.com.crawlerglobo.uil.CrawlerConstants;

/**
 * @author jgosilva
 *
 */

public class LeitorRSSFeedGlobo {

	private static final Logger logger = Logger.getLogger(LeitorRSSFeedGlobo.class);
    
    final URL url;

	public LeitorRSSFeedGlobo(String feedUrl) {
        try {
            url = new URL(feedUrl);
        } catch (MalformedURLException e) {
        	logger.error("[RSSFeedParser][RSSFeedParser()]Erro ao tentar conectar na URL "+ feedUrl);
        	throw new RuntimeException(e);
            
        }
    }

    public Feed readFeed() {
        Feed feed = null;
        try {
            boolean isFeedHeader = true;
            String description = "";
            String title = "";
            String link = "";

            XMLEventReader eventReader = xmlDocument();
            
            while (eventReader.hasNext()) {
                XMLEvent event = eventReader.nextEvent();
                if (event.isStartElement()) {
                	String localPart = event.asStartElement().getName().getLocalPart();
                    switch (localPart) {
                    case CrawlerConstants.ITEM:
                        if (isFeedHeader) {
                            isFeedHeader = false;
                            feed = new Feed();
                        }
                        event = eventReader.nextEvent();
                        break;
                    case CrawlerConstants.TITLE:
                        title = getCharacterData(event, eventReader);
                        break;
                    case CrawlerConstants.DESCRIPTION:
                        description = getCharacterData(event, eventReader);
                        break;
                    case CrawlerConstants.LINK:
                        link = getCharacterData(event, eventReader);
                        break;
                    }
                } else if (event.isEndElement()) {
                    if (event.asEndElement().getName().getLocalPart() == (CrawlerConstants.ITEM)) {
                        Item message = getItemList(description, title, link);
                        feed.getMessages().add(message);
                        event = eventReader.nextEvent();
                        continue;
                    }
                }
            }
        } catch (XMLStreamException e) {
        	logger.error("[RSSFeedParser][readFeed()]Erro ao tentar ler o feed");
            throw new RuntimeException(e);
        }
        return feed;
    }

	private XMLEventReader xmlDocument() throws FactoryConfigurationError, XMLStreamException {
		XMLInputFactory inputFactory = XMLInputFactory.newInstance();
		InputStream in = read();
		XMLEventReader eventReader = inputFactory.createXMLEventReader(in);
		return eventReader;
	}

	private Item getItemList(String description, String title, String link) {
		Item message = new Item();
		List<Description> lista = getContent(description);
		message.setDescription(lista);
		message.setLink(link);
		message.setTitle(title);
		return message;
	}
    
    private List<Description> getContent(String html){
    	List<Description> lista = new ArrayList<Description>(); 
    	lista.addAll(ConvertHtml.getContentParagraph(html));
    	lista.addAll(ConvertHtml.getContentImg(html));
    	lista.addAll(ConvertHtml.getContentUl(html));
    	return lista;
    }
   
  
	private String getCharacterData(XMLEvent event, XMLEventReader eventReader)throws XMLStreamException {
        String result = "";
        event = eventReader.nextEvent();
        if (event instanceof Characters) {
            result = event.asCharacters().getData();
        }
        return result;
    }

    private InputStream read() {
        try {
            return url.openStream();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
