package br.com.crawlerglobo.uil;

public class CrawlerConstants {

	public static final String URL = "http://revistaautoesporte.globo.com/rss/ultimas/feed.xml";
	public static final String REGEX_TAG_P = "<p>([\\w\\W]+?)</p>";
	public static final String REGEX_TAG_IMG = "<img([\\s\\w\\W]+?)/>";
	public static final String REGEX_TAG_UL = "<ul>([\\s\\w\\W]+?)</ul>";
	public static final String REGEX_TAG_UL_LINK = "href=\"[^>]*\">";
	public static final String TYPE_UL_LINK = "links";
	public static final String TYPE_IMAGE = "image";
	public static final String REGEX_IMAGE_LINK = "http:[^\\']+";
	public static final String TYPE_TEXT = "text";
	public static final String TITLE = "title";
	public static final String DESCRIPTION = "description";
	public static final String LINK = "link";
	public static final String ITEM = "item";
}
